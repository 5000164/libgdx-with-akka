package models.ball

import models.core.Entity
import models.vobjs.Pos

case class Ball(x: Float, y: Float) extends AnyRef with Entity[Ball] {
  lazy val pos = Pos(x, y)
  override def moved(dx: Float, dy: Float): Ball = copy(x + dx, y + dy)
}
