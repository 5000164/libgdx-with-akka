package models.ball

import akka.actor.Actor
import models.events.Update

trait BallActor extends Actor {
  private var ball: Ball = initialize()
  def initialize(): Ball
  def update(ball: Ball): Ball
  override def receive: Receive = {
    case Update => ball = update(ball)
  }
}
