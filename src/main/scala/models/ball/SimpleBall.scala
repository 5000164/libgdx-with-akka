package models.ball

import akka.actor.ActorRef
import models.core.MovableActor
import models.events.Updated
import models.vobjs.{Area, Speed}

class SimpleBall(manager: ActorRef, ball: Ball, val speed: Speed, val area: Area) extends BallActor with MovableActor[Ball] {
  def updated(ball: Ball): Ball = {
    manager ! Updated(ball)
    ball
  }
  def updated(ball: Ball, fs: (Ball => Ball)*): Ball = updated(fs.foldLeft(ball)((ball, f) => f(ball)))
  override def initialize(): Ball = ball
  override def update(ball: Ball): Ball = updated(ball, move)
}
