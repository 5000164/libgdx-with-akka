package models.core

import models.events.Update

trait UpdatableActor[E <: Entity[E]] {
  private[core] var entity: E = initialize()
  def initialize(): E
  def update(entity: E): E
  def updated(entity: E): Unit
  def modified(entity: E, fs: (E => E)*): E = fs.foldLeft(entity)((entity, f) => f(entity))
  val updateHandler: PartialFunction[Any, Unit] = {
    case Update =>
      entity = update(entity)
      updated(entity)
  }
}
