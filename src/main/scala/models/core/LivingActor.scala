package models.core

import models.events.Damage

trait LivingActor[E <: Entity[E]] {
  self: UpdatableActor[E] =>
  def damaged(entity: E, damage: Int): E
  val damageHandler: PartialFunction[Any, Unit] = {
    case Damage(damage: Int) => entity = damaged(entity, damage)
  }
}
