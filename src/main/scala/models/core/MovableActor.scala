package models.core

import akka.actor.Actor
import models.vobjs.Speed

trait MovableActor[E <: Entity[E]] {
  self: Actor =>
  def speed: Speed
  implicit class Movable(entity: E) {
    def move(x: Float, y: Float) = entity.moved(x, y)
  }
  def move(entity: E) = entity.moved(speed.x, speed.y)
}
