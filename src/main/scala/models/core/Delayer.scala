package models.core

trait Delayer[S, T] {
  private var target: T = initialize()
  def initialize(): T
  def updateTarget(newTarget: T) = target = newTarget
  def update(): S = merge(target)
  def merge(target: T): S
}
