package models.core

trait Entity[+T] {
  self: T =>
  val x: Float
  val y: Float
  def moved(dx: Float, dy: Float): T
}
