package models.core

trait Charger[C <: Chargeable[C]] {
  def charge(target: C): C = target.charged(target.charge.update())
}
