package models.core

import models.vobjs.Charge

trait Chargeable[T] {
  val charge: Charge
  def charged(newCharge: Charge): T
  def flush() = charged(charge.zero)
}
