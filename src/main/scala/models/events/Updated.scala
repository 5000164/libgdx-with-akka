package models.events

import models.core.Entity

case class Updated[T](entity: Entity[T]) extends AppEvent
