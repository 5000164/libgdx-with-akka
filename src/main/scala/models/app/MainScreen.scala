package models.app

import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.{Gdx, Screen}
import models.vobjs.Grid

class MainScreen(implicit val grid: Grid) extends Screen {
  val shapeRenderer = new ShapeRenderer()
  val batch = new SpriteBatch()

  override def hide(): Unit = {}
  override def resize(width: Int, height: Int): Unit = {}
  override def dispose(): Unit = {}
  override def pause(): Unit = {}
  override def render(delta: Float): Unit = {}
  def render(model: Model): Unit = {
    Gdx.gl.glClearColor(0, 0, 0, 1)
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

    shapeRenderer.begin(ShapeRenderer.ShapeType.Line)
    shapeRenderer.setColor(1, 1, 0, 1)
    model.balls.foreach { case (_, ball) =>
      shapeRenderer.circle(ball.x, ball.y, 3)
    }
    model.cannons.foreach { case (_, cannon) =>
      shapeRenderer.setColor(0, 0, 1, 1)
      shapeRenderer.rect(cannon.x - grid.size / 4, cannon.y, grid.size / 2, grid.size)
      shapeRenderer.setColor(0, 1, 0, 1)
      cannon.charge.copy(current = cannon.charge.current - 20, max = 70).calc(grid.size - 4) match {
        case 0 =>
        case i =>
          shapeRenderer.rect(cannon.x - grid.size / 4 + 2, cannon.y + 2, grid.size / 2 - 4, i)
      }
    }
    shapeRenderer.setColor(1, 0, 0, 1)
    model.enemies.foreach { case (_, enemy) =>
      shapeRenderer.circle(enemy.x, enemy.y, enemy.life.calc(10))
    }
    model.currentTile.foreach { pos =>
      shapeRenderer.setColor(1, 1, 1, 1)
      pos foreach { (x, y) =>
        shapeRenderer.rect(x, y, grid.size, grid.size)
      }
    }
    shapeRenderer.end()
  }
  override def show(): Unit = {}
  override def resume(): Unit = {}
}
