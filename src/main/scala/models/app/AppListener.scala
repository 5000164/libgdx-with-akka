package models.app

import akka.actor.ActorRef
import com.badlogic.gdx.{Game, Gdx, InputProcessor}
import models.events.{TouchDown, Update}
import models.vobjs.Grid

import scala.collection.concurrent.TrieMap

class AppListener(manager: ActorRef)(implicit val grid: Grid) extends Game with InputProcessor {
  var model = Model(TrieMap.empty, TrieMap.empty, TrieMap.empty, None, PointModel(0))
  var initialized = false

  override def render(): Unit = {
    manager ! Update
    s.foreach(_.render(model))
  }
  override def create(): Unit = {
    setScreen(new MainScreen)
    Gdx.input.setInputProcessor(this)
  }
  override def dispose(): Unit = {
    super.dispose()
  }
  def s = Option[MainScreen](screen.asInstanceOf[MainScreen])
  override def mouseMoved(screenX: Int, screenY: Int): Boolean = false
  override def keyTyped(character: Char): Boolean = false
  override def keyDown(keycode: Int): Boolean = false
  override def touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean = {
    manager ! TouchDown(screenX, screenY)
    false
  }
  override def keyUp(keycode: Int): Boolean = false
  override def scrolled(amount: Int): Boolean = false
  override def touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean = false
  override def touchDragged(screenX: Int, screenY: Int, pointer: Int): Boolean = false
}
