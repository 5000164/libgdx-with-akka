package models.app

import akka.actor.ActorRef
import models.ball.Ball
import models.cannon.Cannon
import models.enemy.Enemy

import scala.collection.concurrent.TrieMap

case class Model(
                  balls: TrieMap[ActorRef, Ball],
                  cannons: TrieMap[ActorRef, Cannon],
                  enemies: TrieMap[ActorRef, Enemy],
                  currentTile: Option[CurrentTile],
                  point: PointModel
                )

case class EntityModel(balls: TrieMap[ActorRef, Ball], cannons: TrieMap[ActorRef, Cannon], enemies: TrieMap[ActorRef, Enemy])
case class PointModel(current: Int, max: Int = 100, stock: Int = 5, stockMax: Int = 10)
