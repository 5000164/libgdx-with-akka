package models.app

import models.core.Delayer
import models.vobjs.Pos

class CurrentTile(pos: Pos) extends Pos(pos.x, pos.y) with Delayer[CurrentTile, Pos] {
  override def initialize(): Pos = pos
  override def merge(target: Pos): CurrentTile = new CurrentTile(Pos(Math.round(pos.x + (target.x - pos.x) / 4), Math.round(pos.y + (target.y - pos.y) / 4)))
}
