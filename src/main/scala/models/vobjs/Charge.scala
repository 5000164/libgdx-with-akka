package models.vobjs

case class Charge(current: Int, max: Int, min: Int = 0, delta: Int = 1) {
  def update() = if (current == max) this else copy(current = (current + delta).min(max))
  def calc(total: Int) = total.toFloat * (current.min(max).max(min).toFloat / max)
  def isMax = current == max
  def zero = copy(current = 0)
}
