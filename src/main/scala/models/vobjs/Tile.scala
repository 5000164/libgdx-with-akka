package models.vobjs

case class Tile(x: Int, y: Int) {
  def toPos(grid: Grid): Pos = Pos(x * grid.size, y * grid.size)
}
