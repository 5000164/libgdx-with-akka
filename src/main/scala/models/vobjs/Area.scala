package models.vobjs

case class Area(x: Int, y: Int, width: Int, height: Int) {
  def contain(pos: Pos) = pos.x >= x && pos.y >= y && pos.x <= x + width && pos.y <= y + height
}
