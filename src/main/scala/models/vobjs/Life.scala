package models.vobjs

case class Life(current: Int, max: Int) {
  def damaged(d: Int) = copy(current = (current - d).max(0))
  def isAlive = current > 0
  def calc(total: Int) = total.toFloat * (current.toFloat / max)
}
