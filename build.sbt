name := "minimum-libgdx"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  "com.badlogicgames.gdx" % "gdx-backend-lwjgl" % "1.9.4",
  "com.badlogicgames.gdx" % "gdx" % "1.9.4",
  "com.badlogicgames.gdx" % "gdx-platform" % "1.9.4" classifier "natives-desktop",
  "com.typesafe" % "config" % "1.3.1",
  "com.typesafe.akka" %% "akka-actor" % "2.4.11"
)